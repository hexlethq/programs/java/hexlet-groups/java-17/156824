package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String longSentence) {
        String[] wordIndex = longSentence.split(" ");
        String abbr = "";
        for (String s : wordIndex) {
            abbr += s.substring(0, 1).toUpperCase();
        }
        return abbr;
    }
}
    // END

