package exercise;

class Triangle {
    // BEGIN
    public static final double PI = Math.PI;
    public static double getSquare (int a, int b, double angle) {
        angle = (angle * PI) / 180;
        return Math.sin(angle) * (a * b) / 2;
    }
   public static void main(String[] args) {
                System.out.println(getSquare(4, 5, 45));
    }
}
    // END

