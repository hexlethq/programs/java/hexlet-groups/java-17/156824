package exercise;

class Converter {

    // BEGIN
    public static int convert(int value, String format) {
        if (format.equals("b")) {
            return value * 1024;
        }
        if (format.equals("Kb")) {
           return value / 1024;
        }
        else {
          return 0;
        }
    }
    public static void main(String[] args) {
        int valueTest = 10;
        String formatTest = "b";
        int resultConvert = convert(valueTest,formatTest);
        String output_b = String.format("%d Kb = " + resultConvert + " b", valueTest);
        String output_Kb = String.format("%d b = " + resultConvert + " kB", valueTest);
        System.out.println(output_b);
    }
}
    // END


