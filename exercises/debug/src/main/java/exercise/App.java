package exercise;

class App {
    // BEGIN
    public static final String VER_TRIANGLE = "Разносторонний";
    public static final String EQ_TRIANGLE = "Равносторонний";
    public static final String ISO_TRIANGLE = "Равнобедренный";
    public static final String NO_TRIANGLE = "Треугольник не существует";

    public static String getTypeOfTriangle(int a, int b, int c) {
        if ((a + b) > c && (b + c) > a && (c + a) > b) {
            if (a != b && b != c && a != c) {
                return VER_TRIANGLE;
            }
            if (a == b && a == c) {
                return EQ_TRIANGLE;
            }
            if (a == b || b == c || c == a) {
                return ISO_TRIANGLE;
            }
        }
        return NO_TRIANGLE;
    }
}
        // END

