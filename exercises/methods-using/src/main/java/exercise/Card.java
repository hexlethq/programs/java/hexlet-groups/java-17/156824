package exercise;

class Card {
    public static void printHiddenCard(String cardNumber, int starsCount) {
        // BEGIN
        String cutCard = cardNumber.substring(12);
        String star = "*";
        String hideCard = star.repeat(starsCount);
        System.out.println(hideCard + cutCard);
        // END
    }
}
